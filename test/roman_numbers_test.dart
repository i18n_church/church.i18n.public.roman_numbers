import 'dart:math';

import 'package:roman_numbers/roman_numbers.dart';
import 'package:test/test.dart';

void main() {
  group('A group of tests', () {
    const _romanDecimals = {
      'N': 0, 'n': 0, //
      'I': 1, 'i': 1, 'Ⅰ': 1, 'ⅰ': 1, //
      'Ⅱ': 2, 'ⅱ': 2, //
      'Ⅲ': 3, 'ⅲ': 3, //
      'Ⅳ': 4, 'ⅳ': 4, //
      'V': 5, 'v': 5, 'Ⅴ': 5, 'ⅴ': 5, //
      'Ⅵ': 6, 'ⅵ': 6, 'ↅ': 6, //
      'Ⅶ': 7, 'ⅶ': 7, //
      'Ⅷ': 8, 'ⅷ': 8, //
      'Ⅸ': 9, 'ⅸ': 9, //
      'X': 10, 'x': 10, 'Ⅹ': 10, 'ⅹ': 10, //
      'Ⅺ': 11, 'ⅺ': 11, //
      'Ⅻ': 12, 'ⅻ': 12, //
      'L': 50, 'l': 50, 'Ⅼ': 50, 'ⅼ': 50, 'ↆ': 50, //
      'C': 100, 'c': 100, 'Ⅽ': 100, 'ⅽ': 100, //
      'D': 500, 'd': 500, 'Ⅾ': 500, 'ⅾ': 500, //
      'M': 1000, 'm': 1000, 'Ī': 1000, 'ī': 1000, 'Ⅿ': 1000, 'ⅿ': 1000, 'ↀ': 1000, //
      'ↁ': 5000, 'ↂ': 10000, 'ↇ': 50000, 'ↈ': 100000 //
    };

    test('Roman decimals, all characters', () {
      _romanDecimals.forEach((key, value) {
        final arabic = RomanNumbers.fromRoman(key);
        expect(arabic, equals(value), reason: 'Value [$key] does not match. Arabic: $arabic');
      });
    });

    const _eachLength = {
      0: 'N',
      2: 'II',
      3: 'III',
      8: 'VIII',
      18: 'XVIII',
      28: 'XXVIII',
      38: 'XXXVIII',
      88: 'LXXXVIII',
      188: 'CLXXXVIII',
      288: 'CCLXXXVIII',
      388: 'CCCLXXXVIII',
      888: 'DCCCLXXXVIII',
      1888: 'MDCCCLXXXVIII',
      2888: 'MMDCCCLXXXVIII',
      3888: 'MMMDCCCLXXXVIII',
      4888: 'I̅V̅DCCCLXXXVIII',
      7388: 'V̅I̅I̅CCCLXXXVIII',
      7888: 'V̅I̅I̅DCCCLXXXVIII',
      8388: 'V̅I̅I̅I̅CCCLXXXVIII',
      8888: 'V̅I̅I̅I̅DCCCLXXXVIII',
      18388: 'X̅V̅I̅I̅I̅CCCLXXXVIII',
      18888: 'X̅V̅I̅I̅I̅DCCCLXXXVIII',
      28388: 'X̅X̅V̅I̅I̅I̅CCCLXXXVIII',
      28888: 'X̅X̅V̅I̅I̅I̅DCCCLXXXVIII',
      38388: 'X̅X̅X̅V̅I̅I̅I̅CCCLXXXVIII',
      38888: 'X̅X̅X̅V̅I̅I̅I̅DCCCLXXXVIII',
      88388: 'L̅X̅X̅X̅V̅I̅I̅I̅CCCLXXXVIII',
      88888: 'L̅X̅X̅X̅V̅I̅I̅I̅DCCCLXXXVIII',
      188388: 'C̅L̅X̅X̅X̅V̅I̅I̅I̅CCCLXXXVIII',
      188888: 'C̅L̅X̅X̅X̅V̅I̅I̅I̅DCCCLXXXVIII',
      288388: 'C̅C̅L̅X̅X̅X̅V̅I̅I̅I̅CCCLXXXVIII',
      288888: 'C̅C̅L̅X̅X̅X̅V̅I̅I̅I̅DCCCLXXXVIII',
      388388: 'C̅C̅C̅L̅X̅X̅X̅V̅I̅I̅I̅CCCLXXXVIII',
      388888: 'C̅C̅C̅L̅X̅X̅X̅V̅I̅I̅I̅DCCCLXXXVIII',
      888388: 'D̅C̅C̅C̅L̅X̅X̅X̅V̅I̅I̅I̅CCCLXXXVIII',
      888888: 'D̅C̅C̅C̅L̅X̅X̅X̅V̅I̅I̅I̅DCCCLXXXVIII',
      1888388: 'M̅D̅C̅C̅C̅L̅X̅X̅X̅V̅I̅I̅I̅CCCLXXXVIII',
      1888888: 'M̅D̅C̅C̅C̅L̅X̅X̅X̅V̅I̅I̅I̅DCCCLXXXVIII',
      2888388: 'M̅M̅D̅C̅C̅C̅L̅X̅X̅X̅V̅I̅I̅I̅CCCLXXXVIII',
      2888888: 'M̅M̅D̅C̅C̅C̅L̅X̅X̅X̅V̅I̅I̅I̅DCCCLXXXVIII',
      3888388: 'M̅M̅M̅D̅C̅C̅C̅L̅X̅X̅X̅V̅I̅I̅I̅CCCLXXXVIII',
      3888888: 'M̅M̅M̅D̅C̅C̅C̅L̅X̅X̅X̅V̅I̅I̅I̅DCCCLXXXVIII',
      8888388: 'V̿I̿I̿I̿D̅C̅C̅C̅L̅X̅X̅X̅V̅I̅I̅I̅CCCLXXXVIII',
      8888888: 'V̿I̿I̿I̿D̅C̅C̅C̅L̅X̅X̅X̅V̅I̅I̅I̅DCCCLXXXVIII',
      18888388: 'X̿V̿I̿I̿I̿D̅C̅C̅C̅L̅X̅X̅X̅V̅I̅I̅I̅CCCLXXXVIII',
      18888888: 'X̿V̿I̿I̿I̿D̅C̅C̅C̅L̅X̅X̅X̅V̅I̅I̅I̅DCCCLXXXVIII',
      28888388: 'X̿X̿V̿I̿I̿I̿D̅C̅C̅C̅L̅X̅X̅X̅V̅I̅I̅I̅CCCLXXXVIII',
      28888888: 'X̿X̿V̿I̿I̿I̿D̅C̅C̅C̅L̅X̅X̅X̅V̅I̅I̅I̅DCCCLXXXVIII',
      38888388: 'X̿X̿X̿V̿I̿I̿I̿D̅C̅C̅C̅L̅X̅X̅X̅V̅I̅I̅I̅CCCLXXXVIII',
      38888888: 'X̿X̿X̿V̿I̿I̿I̿D̅C̅C̅C̅L̅X̅X̅X̅V̅I̅I̅I̅DCCCLXXXVIII',
      88888388: 'L̿X̿X̿X̿V̿I̿I̿I̿D̅C̅C̅C̅L̅X̅X̅X̅V̅I̅I̅I̅CCCLXXXVIII',
      88888888: 'L̿X̿X̿X̿V̿I̿I̿I̿D̅C̅C̅C̅L̅X̅X̅X̅V̅I̅I̅I̅DCCCLXXXVIII',
      188888388: 'C̿L̿X̿X̿X̿V̿I̿I̿I̿D̅C̅C̅C̅L̅X̅X̅X̅V̅I̅I̅I̅CCCLXXXVIII',
      188888888: 'C̿L̿X̿X̿X̿V̿I̿I̿I̿D̅C̅C̅C̅L̅X̅X̅X̅V̅I̅I̅I̅DCCCLXXXVIII',
      288888388: 'C̿C̿L̿X̿X̿X̿V̿I̿I̿I̿D̅C̅C̅C̅L̅X̅X̅X̅V̅I̅I̅I̅CCCLXXXVIII',
      288888888: 'C̿C̿L̿X̿X̿X̿V̿I̿I̿I̿D̅C̅C̅C̅L̅X̅X̅X̅V̅I̅I̅I̅DCCCLXXXVIII',
      388888388: 'C̿C̿C̿L̿X̿X̿X̿V̿I̿I̿I̿D̅C̅C̅C̅L̅X̅X̅X̅V̅I̅I̅I̅CCCLXXXVIII',
      388888888: 'C̿C̿C̿L̿X̿X̿X̿V̿I̿I̿I̿D̅C̅C̅C̅L̅X̅X̅X̅V̅I̅I̅I̅DCCCLXXXVIII',
      888888388: 'D̿C̿C̿C̿L̿X̿X̿X̿V̿I̿I̿I̿D̅C̅C̅C̅L̅X̅X̅X̅V̅I̅I̅I̅CCCLXXXVIII',
      888888888: 'D̿C̿C̿C̿L̿X̿X̿X̿V̿I̿I̿I̿D̅C̅C̅C̅L̅X̅X̅X̅V̅I̅I̅I̅DCCCLXXXVIII',
      1888888388: 'M̿D̿C̿C̿C̿L̿X̿X̿X̿V̿I̿I̿I̿D̅C̅C̅C̅L̅X̅X̅X̅V̅I̅I̅I̅CCCLXXXVIII',
      1888888888: 'M̿D̿C̿C̿C̿L̿X̿X̿X̿V̿I̿I̿I̿D̅C̅C̅C̅L̅X̅X̅X̅V̅I̅I̅I̅DCCCLXXXVIII',
      2888888388: 'M̿M̿D̿C̿C̿C̿L̿X̿X̿X̿V̿I̿I̿I̿D̅C̅C̅C̅L̅X̅X̅X̅V̅I̅I̅I̅CCCLXXXVIII',
      2888888888: 'M̿M̿D̿C̿C̿C̿L̿X̿X̿X̿V̿I̿I̿I̿D̅C̅C̅C̅L̅X̅X̅X̅V̅I̅I̅I̅DCCCLXXXVIII',
      3888888388: 'M̿M̿M̿D̿C̿C̿C̿L̿X̿X̿X̿V̿I̿I̿I̿D̅C̅C̅C̅L̅X̅X̅X̅V̅I̅I̅I̅CCCLXXXVIII',
      3888888888: 'M̿M̿M̿D̿C̿C̿C̿L̿X̿X̿X̿V̿I̿I̿I̿D̅C̅C̅C̅L̅X̅X̅X̅V̅I̅I̅I̅DCCCLXXXVIII',
      4888888388: 'M̿M̿M̿M̿D̿C̿C̿C̿L̿X̿X̿X̿V̿I̿I̿I̿D̅C̅C̅C̅L̅X̅X̅X̅V̅I̅I̅I̅CCCLXXXVIII',
      4888888888: 'M̿M̿M̿M̿D̿C̿C̿C̿L̿X̿X̿X̿V̿I̿I̿I̿D̅C̅C̅C̅L̅X̅X̅X̅V̅I̅I̅I̅DCCCLXXXVIII'
    };

    test('All distinct length up to 5 billion', () {
      _eachLength.forEach((key, value) {
        final roman = RomanNumbers.fromRoman(value);
        expect(roman, equals(key), reason: 'Value [$key] does not match. Roman: $roman');
      });
    });

    test('First 5 million values: Arabic -> Roman -> Arabic', () {
      for (var i = 0; i < 5000005; i++) {
        final roman = RomanNumbers.toRoman(i);
        expect(roman, isNotNull);
        final arabic = RomanNumbers.fromRoman(roman);
        expect(arabic, equals(i), reason: 'Value [$i] does not match. Roman: $roman Arabic: $arabic');
      }
    });

    test('Randomly between 5 million and ~5 billion: Arabic -> Roman -> Arabic', () {
      final r = Random();
      for (var i = 0; i < 1000000; i++) {
        final nextRandom = r.nextInt(4294967296) + 5000000;
        final roman = RomanNumbers.toRoman(nextRandom);
        expect(roman, isNotNull, reason: 'Failed to convert number ');
        final arabic = RomanNumbers.fromRoman(roman);
        expect(arabic, equals(nextRandom), reason: 'Value [$nextRandom] does not match. Roman: $roman Arabic: $arabic');
      }
    });

    test('Not a Roman number', () {
      expect(() => RomanNumbers.fromRoman('XVIPRG'), throwsArgumentError, reason: 'Value "XVIPRG" was converted.');
    });

    test('Negative number', () {
      expect(() => RomanNumbers.toRoman(-1), throwsArgumentError, reason: 'Negative number -1 was converted.');
    });

    test('5 Billion number', () {
      expect(() => RomanNumbers.toRoman(5000000000), throwsArgumentError, reason: 'Number 5 billion was converted.');
    });
  });
}
