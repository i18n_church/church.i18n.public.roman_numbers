import 'package:roman_numbers/roman_numbers.dart';

void main() {
  var arabic = RomanNumbers.fromRoman('MMXXI');
  print('MMXXI converted to arabic: $arabic');
  assert(arabic == 2021);
  var roman = RomanNumbers.toRoman(2021);
  print('2021 converted to roman: $roman');
  assert(roman == 'MMXXI');

  //large numbers
  arabic = RomanNumbers.fromRoman('M̿M̿M̿M̿D̿C̿C̿C̿L̿X̿X̿X̿V̿I̿I̿I̿D̅C̅C̅C̅L̅X̅X̅X̅V̅I̅I̅I̅DCCCLXXXVIII');
  print('M̿M̿M̿M̿D̿C̿C̿C̿L̿X̿X̿X̿V̿I̿I̿I̿D̅C̅C̅C̅L̅X̅X̅X̅V̅I̅I̅I̅DCCCLXXXVIII converted to arabic: $arabic');
  assert(arabic == 4888888888);

  roman = RomanNumbers.toRoman(4732156498);
  print('4732156498 converted to roman: $roman');
  assert(roman == 'M̿M̿M̿M̿D̿C̿C̿X̿X̿X̿M̅M̅C̅L̅V̅I̅CDXCVIII');
}
