Library converts Arabic numbers into Roman and vice versa. It supports range
starting from zero (including) up to 5 billion (excluding).

## Usage

A simple usage example:

```dart
import 'package:roman_numbers/roman_numbers.dart';

main() {
  var arabic = RomanNumbers.fromRoman('MMXXI');
  print('MMXXI converted to arabic: $arabic');
  assert(arabic == 2021);
  var roman = RomanNumbers.toRoman(2021);
  print('2021 converted to roman: $roman');
  assert(roman == 'MMXXI');

  //large numbers
  arabic = RomanNumbers.fromRoman('M̿M̿M̿M̿D̿C̿C̿C̿L̿X̿X̿X̿V̿I̿I̿I̿D̅C̅C̅C̅L̅X̅X̅X̅V̅I̅I̅I̅DCCCLXXXVIII');
  print('M̿M̿M̿M̿D̿C̿C̿C̿L̿X̿X̿X̿V̿I̿I̿I̿D̅C̅C̅C̅L̅X̅X̅X̅V̅I̅I̅I̅DCCCLXXXVIII converted to arabic: $arabic');
  assert(arabic == 4888888888);

  roman = RomanNumbers.toRoman(4732156498);
  print('4732156498 converted to roman: $roman');
  assert(roman == 'M̿M̿M̿M̿D̿C̿C̿X̿X̿X̿M̅M̅C̅L̅V̅I̅CDXCVIII');
}
```
## Details

Implementation supports various unicode characters for conversion:

* **0**: N, n
* **1**: I, i, Ⅰ, ⅰ
* **2**: Ⅱ, ⅱ
* **3**: Ⅲ, ⅲ
* **4**: Ⅳ, ⅳ
* **5**: V, v, Ⅴ, ⅴ
* **6**: Ⅵ, ⅵ, ↅ
* **7**: Ⅶ, ⅶ
* **8**: Ⅷ, ⅷ
* **9**: Ⅸ, ⅸ
* **10**: X, x, Ⅹ, ⅹ
* **11**: Ⅺ, ⅺ
* **12**: Ⅻ, ⅻ
* **50**: L, l, Ⅼ, ⅼ, ↆ
* **100**: C, c, Ⅽ, ⅽ
* **500**: D, d, Ⅾ, ⅾ
* **1000**: M, m, Ī, ī, Ⅿ, ⅿ, ↀ
* **5000**: ↁ
* **10000**: ↂ
* **50000**: ↇ
* **100000**: ↈ

Characters:

* ̅  (U+0304 COMBINING MACRON) and
* ̅	(U+0305 COMBINING OVERLINE) are used as thousands multiplier and 
* ̿ (U+033F COMBINING DOUBLE OVERLINE) as millions multiplier.
   E.g. V̅ is 5000, C̅ = 100000 or M̿ is one billion.
   
According to the [Proposal to Add Additional Ancient Roman Characters to UCS][proposal]
there are more characters and multipliers that were used, however they are not part of
the Unicode characters set.

[proposal]: http://std.dkuug.dk/jtc1/sc2/wg2/docs/n3218.pdf

## Features and bugs

Please file feature requests and bugs at the [issue tracker][tracker].

[tracker]: https://bitbucket.org/i18n_church/church.i18n.public.roman_numbers/issues?status=new&status=open
