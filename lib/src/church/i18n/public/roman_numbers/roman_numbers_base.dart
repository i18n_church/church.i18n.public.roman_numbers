/// Library converts Arabic numbers into Roman and vice versa. It supports range
// starting from zero (including) up to 5 billion (excluding). Implementation
/// supports various unicode characters for conversion:
/// 0: N, n
/// 1: I, i, Ⅰ, ⅰ
/// 2: Ⅱ, ⅱ
/// 3: Ⅲ, ⅲ
/// 4: Ⅳ, ⅳ
/// 5: V, v, Ⅴ, ⅴ
/// 6: Ⅵ, ⅵ, ↅ
/// 7: Ⅶ, ⅶ
/// 8: Ⅷ, ⅷ
/// 9: Ⅸ, ⅸ
/// 10: X, x, Ⅹ, ⅹ
/// 11: Ⅺ, ⅺ
/// 12: Ⅻ, ⅻ
/// 50: L, l, Ⅼ, ⅼ, ↆ
/// 100: C, c, Ⅽ, ⅽ
/// 500: D, d, Ⅾ, ⅾ
/// 1000: M, m, Ī, ī, Ⅿ, ⅿ, ↀ
/// 5000: ↁ
/// 10000: ↂ
/// 50000: ↇ
/// 100000: ↈ
/// Characters:
///   '̅' (U+0304 COMBINING MACRON) and
///   '̅'	(U+0305 COMBINING OVERLINE)
/// are used as thousands multiplier and '̿' (U+033F COMBINING DOUBLE OVERLINE)
/// as a millions multiplier. E.g. V̅ is 5000, C̅ = 100000 or M̿ is one billion.
/// According to the [Proposal to Add Additional Ancient Roman Characters to UCS][proposal]
/// there are more characters and multipliers that were used, however they are not part of
/// the Unicode characters set.
///[proposal]: http://std.dkuug.dk/jtc1/sc2/wg2/docs/n3218.pdf
class RomanNumbers {
  static const List<List<String>> _romanNumerals = [
    ['', 'I', 'II', 'III', 'IV', 'V', 'VI', 'VII', 'VIII', 'IX'], //ones
    ['', 'X', 'XX', 'XXX', 'XL', 'L', 'LX', 'LXX', 'LXXX', 'XC'], //tens
    ['', 'C', 'CC', 'CCC', 'CD', 'D', 'DC', 'DCC', 'DCCC', 'CM'], //hundreds
    ['', 'M', 'MM', 'MMM', 'I̅V̅', 'V̅', 'V̅I̅', 'V̅I̅I̅', 'V̅I̅I̅I̅', 'I̅X̅'], //thousands
    ['', 'X̅', 'X̅X̅', 'X̅X̅X̅', 'X̅L̅', 'L̅', 'L̅X̅', 'L̅X̅X̅', 'L̅X̅X̅X̅', 'X̅C̅'], //ten thousands
    ['', 'C̅', 'C̅C̅', 'C̅C̅C̅', 'C̅D̅', 'D̅', 'D̅C̅', 'D̅C̅C̅', 'D̅C̅C̅C̅', 'C̅M̅'], //hundred thousands
    ['', 'M̅', 'M̅M̅', 'M̅M̅M̅', 'I̿V̿', 'V̿', 'V̿I̿', 'V̿I̿I̿', 'V̿I̿I̿I̿', 'I̿X̿'], //millions
    ['', 'X̿', 'X̿X̿', 'X̿X̿X̿', 'X̿L̿', 'L̿', 'L̿X̿', 'L̿X̿X̿', 'L̿X̿X̿X̿', 'X̿C̿'], //ten millions
    ['', 'C̿', 'C̿C̿', 'C̿C̿C̿', 'C̿D̿', 'D̿', 'D̿C̿', 'D̿C̿C̿', 'D̿C̿C̿C̿', 'C̿M̿'], //hundred millions
    ['', 'M̿', 'M̿M̿', 'M̿M̿M̿', 'M̿M̿M̿M̿'], //billion
  ];

  static const Map<String, int> _romanDecimals = {
    'N': 0, 'n': 0,
    'I': 1, 'i': 1, 'Ⅰ': 1, 'ⅰ': 1,
    'Ⅱ': 2, 'ⅱ': 2,
    'Ⅲ': 3, 'ⅲ': 3,
    'Ⅳ': 4, 'ⅳ': 4,
    'V': 5, 'v': 5, 'Ⅴ': 5, 'ⅴ': 5,
    'Ⅵ': 6, 'ⅵ': 6, 'ↅ': 6,
    'Ⅶ': 7, 'ⅶ': 7,
    'Ⅷ': 8, 'ⅷ': 8,
    'Ⅸ': 9, 'ⅸ': 9,
    'X': 10, 'x': 10, 'Ⅹ': 10, 'ⅹ': 10,
    'Ⅺ': 11, 'ⅺ': 11,
    'Ⅻ': 12, 'ⅻ': 12,
    'L': 50, 'l': 50, 'Ⅼ': 50, 'ⅼ': 50, 'ↆ': 50,
    'C': 100, 'c': 100, 'Ⅽ': 100, 'ⅽ': 100,
    'D': 500, 'd': 500, 'Ⅾ': 500, 'ⅾ': 500,
    'M': 1000, 'm': 1000, 'Ī': 1000, 'ī': 1000, 'Ⅿ': 1000, 'ⅿ': 1000, 'ↀ': 1000,
    'ↁ': 5000,
    'ↂ': 10000,
    'ↇ': 50000,
    'ↈ': 100000 //
  };

  static final Map<int, int> _romanDecimalCodes = _romanDecimals.map((key, value) => MapEntry(key.codeUnits[0], value));
  static const int _thousandMacron = 772; //Characters: ̅ (U+0304 COMBINING MACRON)
  static const int _thousandOverline = 773; //Characters: ̅	(U+0305 COMBINING OVERLINE)
  static const int _million = 831; //Characters: ̿ (U+033F COMBINING DOUBLE OVERLINE)

  /// Method converts an arabic number into roman number. Input could be from zero (including) up to 5 billion (excluding).
  /// When the number is out of range, it throws an Argument Error.
  static String toRoman(int number) {
    if (number >= 5000000000 || number < 0) {
      throw ArgumentError(
          'Method can convert only Natural numbers (including zero) up to 5 billion (excluded). Your value: $number');
    }
    if (number == 0) {
      return 'N';
    }
    if (number < 10) {
      return _romanNumerals[0][number];
    }
    var value = number;
    var romanNumber = '';
    for (var i = 0; i < 10; i++) {
      final d = value % 10;
      value = (value / 10).floor();
      if (value == 0 && d == 0) break;
      romanNumber = _romanNumerals[i][d] + romanNumber;
    }
    return romanNumber;
  }

  /// Method converts a roman number into an arabic number. Method converts
  /// numbers starting from zero (including) up to 5 billion (excluding).
  /// When number contains invalid character that could not be converted,
  /// it throw an ArgumentError. The method does not check whether roman number
  /// is valid or not. In the case you enter invalid input, it may return
  /// invalid output. E.g. value 'IIIIIIIVIIIIIIIIX' may return negative number.
  static int fromRoman(String value) {
    if (value.isEmpty) {
      throw ArgumentError('Input value is empty.');
    }
    var result = 0;
    var prevRomanDecimal = 0;
    var multiplier = 1;
    for (var codePoint in value.codeUnits.reversed) {
      if (codePoint == _thousandOverline || codePoint == _thousandMacron) {
        multiplier = 1000;
      } else if (codePoint == _million) {
        multiplier = 1000000;
      } else {
        var romanDecimal = _romanDecimalCodes[codePoint];
        if (romanDecimal == null) {
          throw ArgumentError('Invalid input value: $value');
        } else {
          romanDecimal *= multiplier;
          multiplier = 1;
          if (romanDecimal < prevRomanDecimal) {
            result -= romanDecimal;
          } else {
            result += romanDecimal;
            prevRomanDecimal = romanDecimal;
          }
        }
      }
    }
    return result;
  }
}
